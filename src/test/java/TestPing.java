package test.java;

import org.junit.Test;
import org.junit.runner.JUnitCore;

import de.ftbastler.statussigns.Server;

public class TestPing {

	public static void main(String[] args) {
		JUnitCore.runClasses(TestPing.class);
	}

	@Test 
	public void testMain() {
		Server s = new Server("hg.mooshroom.net", 25565);
		System.out.println(s.toString());
	}
}
