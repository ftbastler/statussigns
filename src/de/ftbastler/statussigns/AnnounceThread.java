package de.ftbastler.statussigns;

import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

class AnnounceThread extends Thread {
	
    @SuppressWarnings("deprecation")
	public void run() {
    	while(StatusSigns.runt) {
    		for(Entry<Location, String> sign : StatusSigns.signs.entrySet()) {
    			if(!StatusSigns.runt)
    				continue;
    			if(sign.getKey().getBlock().getType() != Material.WALL_SIGN && sign.getKey().getBlock().getType() != Material.SIGN_POST) {
    				for(String key : StatusSigns.getInstance().getConfig().getKeys(false)) {
    					ConfigurationSection s = StatusSigns.getInstance().getConfig().getConfigurationSection(key);
    					if(s.getDouble("X") == sign.getKey().getX() && s.getDouble("Y") == sign.getKey().getY() && s.getDouble("Z") == sign.getKey().getZ()) {
    						StatusSigns.getInstance().getConfig().set(s.getCurrentPath(), null);
    						StatusSigns.getInstance().saveConfig();
    						StatusSigns.getInstance().reloadConfig();
    						StatusSigns.signs.remove(sign.getKey());
    						
    						for(Player p : Bukkit.getServer().getOnlinePlayers()) {
    	    					if(p.isOp()) {
    	    						p.sendMessage(ChatColor.RED + "Server status sign unregistered!");
    	    					}
    	    				}
    					}
    				}
    			
    				continue;
    			}
    			
    			Sign s = (Sign) sign.getKey().getBlock().getState();
    			
    			String[] serverstring = sign.getValue().split("%");
    			String servername = serverstring[0];
    			String ip = serverstring[1];
    			Integer port = Integer.parseInt(serverstring[2]);
    			
    			s.setLine(0, ChatColor.BOLD + "Server status");
    			s.setLine(1, servername);
    			s.setLine(2,  ChatColor.MAGIC + "...");
    			s.setLine(3, ChatColor.DARK_GRAY + "Ping...");
				s.update(true);
    			
    			Server server = new Server(ip, port);
    			try {
    				sleep(1000*2);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
    			
    			s.setLine(0, ChatColor.BOLD + "Server status");
    			s.setLine(1, servername);
    			s.setLine(2, "");
    			s.setLine(3, server.isOnline() ? ChatColor.DARK_GREEN + "Online" : ChatColor.DARK_RED + "Offline");
				s.update(true);
				
				org.bukkit.material.Sign matSign = (org.bukkit.material.Sign) sign.getKey().getBlock().getState().getData();
				Block attachedBlock = sign.getKey().getBlock().getRelative(matSign.getAttachedFace());
				
				for(BlockFace face : BlockFace.values()) {
				    Block red = attachedBlock.getRelative(face);
				    if(red.getType() != Material.REDSTONE_WIRE)
				    	continue;
				    
				    if(StatusSigns.redstoneBlocks.contains(red.getLocation()))
				    	StatusSigns.redstoneBlocks.remove(red.getLocation());
				
					red.setData((byte) (server.isOnline() ? 15 : 0), true);
					red.getState().update(true);
					
				}
				for(BlockFace face : BlockFace.values())
					StatusSigns.redstoneBlocks.add(attachedBlock.getRelative(face).getLocation());
    		}
    		try {
				sleep(1000*30);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    	}
    }
    
}
