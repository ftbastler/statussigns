package de.ftbastler.statussigns;

import java.io.IOException;
import java.net.Socket;

public class Server {

	boolean reachable = false;
	String ip;
	int port;
	
	public Server(String ip, int port) {
		this.ip = ip;
		this.port = port;
		
		
		Socket socket = null;
		try {
		    socket = new Socket(ip, port);
		    reachable = true;
		} catch(IOException e) {
			e.printStackTrace();
		} finally {            
		    if (socket != null) 
		    	try { 
		    		socket.close(); 
		    	} catch(IOException e) {
		    		//ignored
		    	}
		}
	}

	public Boolean isOnline() {
		return reachable;
	}
	
	@Override
	public String toString() {
		return ip + ":" + port + " - Reachable: " + reachable;
	}
	
}
