package de.ftbastler.statussigns;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class StatusSigns extends JavaPlugin implements Listener {

	public static HashMap<Location, String> signs = new HashMap<Location, String>();
	public static ArrayList<Location> redstoneBlocks = new ArrayList<Location>();
	public static Boolean runt;
	private static StatusSigns instance;
	private AnnounceThread at;
	
	public static StatusSigns getInstance() {
		return instance;
	}
	
	@Override
	public void onLoad() {
		instance = this;
	}
	
	@Override
	public void onEnable() {
		File configFile = new File(getDataFolder(), "config.yml");
		if(!configFile.exists()) {
			configFile.getParentFile().mkdirs();
			copy(getResource("config.yml"), configFile);
		}
		
		getServer().getPluginManager().registerEvents(this, this);
		
		loadSigns();
		
		runt = true;
		at = new AnnounceThread();
		at.start();
		
		System.out.print(this + " enabled.");
	}
	
	private void loadSigns() {
		signs.clear();
		for(String key : getConfig().getKeys(false)) {
			ConfigurationSection s = getConfig().getConfigurationSection(key);
			signs.put(new Location(Bukkit.getWorld(s.getString("W")), s.getDouble("X"), s.getDouble("Y"), s.getDouble("Z")), s.getString("S"));
		}
	}
	
	@Override
	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
		runt = false;
		System.out.print(this + " disabled.");
	}
	
	private void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK &&
				(event.getClickedBlock().getType() == Material.SIGN_POST || event.getClickedBlock().getType() == Material.WALL_SIGN) &&
				event.getPlayer().isOp()) {
			
			Sign s = (Sign) event.getClickedBlock().getState();
			Random r = new Random();

			if(!s.getLine(0).equalsIgnoreCase("[Status]"))
				return;
			
			String server = s.getLine(1) + "%" + s.getLine(2) + "%" + s.getLine(3);
			String path = r.nextInt(100) + "";
			while(getConfig().getConfigurationSection(path) != null) {
				path = r.nextInt(100) + "";
			}
			
			path = path + ".";
			getConfig().set(path + "S", server);
			getConfig().set(path + "W", event.getClickedBlock().getLocation().getWorld().getName());
			getConfig().set(path + "X", event.getClickedBlock().getLocation().getX());
			getConfig().set(path + "Y", event.getClickedBlock().getLocation().getY());
			getConfig().set(path + "Z", event.getClickedBlock().getLocation().getZ());
			saveConfig();
			reloadConfig();
			
			signs.put(event.getClickedBlock().getLocation(), server);
			
			event.getPlayer().sendMessage(ChatColor.GREEN + "Registered server status sign!");
			event.setCancelled(true);
						
			s.setLine(0, ChatColor.BOLD + "Server status");
			s.setLine(1, "");
			s.setLine(2,  ChatColor.MAGIC + "...");
			s.setLine(3, ChatColor.DARK_GRAY + "Waiting...");
			s.update(true);
		}
	}
	
	@EventHandler
    public void onRedstoneChange(BlockRedstoneEvent event) {
        if(redstoneBlocks.contains(event.getBlock().getLocation()))
        	event.setNewCurrent(event.getOldCurrent());
    }
}
