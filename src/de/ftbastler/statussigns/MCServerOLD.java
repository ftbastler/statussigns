package de.ftbastler.statussigns;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.bukkit.ChatColor;

public class MCServerOLD {

	private String motd;
	private String mpl;
	private String cpl;
	private Boolean on;
	
	public MCServerOLD(String ip, int port) {
			try {
				Socket sock = new Socket(ip, port);
				 
				DataOutputStream out = new DataOutputStream(sock.getOutputStream());
				DataInputStream in = new DataInputStream(sock.getInputStream());
				 
				out.write(0xFE);
				 
				int b;
				StringBuffer str = new StringBuffer();
				while ((b = in.read()) != -1) {
					if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
						str.append((char) b);
						//System.out.println(b + ":" + ((char) b));
					}
				}
				
				String s = str.toString();
				String[] data = s.split(ChatColor.COLOR_CHAR + "");
				mpl = data[0];
				motd = data[1];
				cpl = data[2];
				
				//this.motd = null;
				//this.cpl = null;
				//this.mpl = null;
				this.on = true;
				
				sock.close();
				} catch(IndexOutOfBoundsException ex) {
					System.out.println("Warning: Could not fetch server information.");
					this.motd = null;
					this.motd = null;
					this.motd = null;
					this.on = false;
				} catch (UnknownHostException e) {
					this.motd = null;
					this.motd = null;
					this.motd = null;
					this.on = false;
				} catch (IOException e) {
					this.motd = null;
					this.motd = null;
					this.motd = null;
					this.on = false;
				}
	}
	
	public Boolean isOnline() {
		return this.on;
	}
	
	public String getMotd() {
		return this.motd;
	}
	
	public String getCpl() {
		return this.cpl;
	}
	
	public String getMpl() {
		return this.mpl;
	}
	
}
