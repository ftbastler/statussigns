# StatusSigns #

A plugin to ping any server and show its status on a sign.

Published as a resource on [SpigotMC](http://www.spigotmc.org/resources/statussigns.297/).

### License ###
StatusSigns (and its source code) by ftbastler is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).